package rait.swedbe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rait.swedbe.model.accounts.Account;
import rait.swedbe.model.transaction.Transaction;
import rait.swedbe.services.accounts.AccountsService;
import rait.swedbe.services.transactionservice.TransactionService;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@RestController
public class RequestController {

    @Autowired
    private AccountsService accountsService;

    @Autowired
    private TransactionService transactionService;

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping("/")
    public List<Account> listAccounts(){
        System.out.println("### Reuqest called");
        return accountsService.list();
    }

    @RequestMapping(value = "/addaccount", method = RequestMethod.POST)
    public Account response(@RequestBody Account account){
        String ts = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        account.setAccountNumber("EE"+ts);
        System.out.println(account);
        accountsService.saveOrUpdate(account);
        return account;
    }

    @RequestMapping("/listtransactions")
    public List<Transaction> getAllTransactions(){
        return transactionService.list();
    }

    @RequestMapping(value = "/transfer", method = RequestMethod.POST)
    public Transaction transferMoney(@RequestBody Transaction transaction){
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        transaction.setTimestamp(ts);

        transactionService.transferMoney(transaction.getSender(), transaction.getReceiver(), accountsService, transaction.getAmount());
        transactionService.createTransaction(transaction);
        return transaction;
    }
}
