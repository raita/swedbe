package rait.swedbe.services.accounts;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rait.swedbe.model.accounts.Account;

import java.util.List;

@Service
public class AccountsService {

    @Autowired
    private AccountsRepo accountsRepo;

    public List<Account> list(){
        return accountsRepo.findAll();
    }

    public Account getAccountById(Long id){
        return accountsRepo.findById(id).get();
    }

    public void saveOrUpdate(Account account){
        accountsRepo.save(account);
    }

    public void deleteAccount(Long id){
        accountsRepo.deleteById(id);
    }
}
