package rait.swedbe.services.accounts;

import org.springframework.data.jpa.repository.JpaRepository;
import rait.swedbe.model.accounts.Account;

public interface AccountsRepo extends JpaRepository<Account, Long> {
}
