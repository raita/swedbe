package rait.swedbe.services.transactionservice;

import org.springframework.data.jpa.repository.JpaRepository;
import rait.swedbe.model.transaction.Transaction;

public interface TransactionRepo extends JpaRepository<Transaction, Long> {
}
