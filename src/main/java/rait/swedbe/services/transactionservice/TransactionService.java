package rait.swedbe.services.transactionservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rait.swedbe.model.accounts.Account;
import rait.swedbe.model.transaction.Transaction;
import rait.swedbe.services.accounts.AccountsService;
import rait.swedbe.services.currencieservice.CurrencyConversion;

import java.util.List;

@Service
public class TransactionService {

    @Autowired
    private TransactionRepo transactionRepo;

    CurrencyConversion cc = new CurrencyConversion();

    public List<Transaction> list(){
        return transactionRepo.findAll();
    }

    public void createTransaction(Transaction transaction){
        transactionRepo.save(transaction);
    }

    public void transferMoney(Account sender, Account receiver, AccountsService as, double amount){
        double convertedAmount = sender.getCurrency().equals(receiver.getCurrency()) ? amount
                : cc.convertedCurrency(sender.getCurrency(), receiver.getCurrency(), amount);

        Account newSender = sender;
        newSender.setBalance(sender.getBalance()-amount);
        as.saveOrUpdate(newSender);

        Account newReceiver = receiver;
        newReceiver.setBalance(receiver.getBalance()+convertedAmount);
        as.saveOrUpdate(newReceiver);
    }
}
