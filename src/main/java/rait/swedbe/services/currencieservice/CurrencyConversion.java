package rait.swedbe.services.currencieservice;

import java.util.HashMap;
import java.util.Map;

public class CurrencyConversion {

    public double convertedCurrency(String origin, String dest, double amount){
        Map<String, Double> conversionRates = new HashMap<>();
        conversionRates.put("EURUSD", 1.09);
        conversionRates.put("EURGBP", 0.84);
        conversionRates.put("USDEUR", 0.92);
        conversionRates.put("USDGBP", 0.77);
        conversionRates.put("GBPUSD", 1.29);
        conversionRates.put("GBPEUR", 1.19);

        double rate = conversionRates.get(origin.toUpperCase()+dest.toUpperCase());
        return rate*amount;
    }
}
