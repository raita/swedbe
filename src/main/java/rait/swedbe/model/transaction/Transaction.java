package rait.swedbe.model.transaction;

import lombok.Getter;
import lombok.Setter;
import rait.swedbe.model.accounts.Account;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "TRANSACTIONS")
public class Transaction {
    @Id
    @GeneratedValue( strategy=GenerationType.AUTO )
    @Column(unique = true)
    private Long transactionId;

    @ManyToOne
    @Setter @Getter
    private Account sender;

    @ManyToOne
    @Setter @Getter
    private Account receiver;

    @Column
    @Setter @Getter
    private double amount;

    @Column
    @Setter @Getter
    private Timestamp timestamp;

    @Column
    @Setter @Getter
    private String comment;

    public Transaction(Long transactionId, Account sender, Account receiver) {
        this.transactionId = transactionId;
        this.sender = sender;
        this.receiver = receiver;
    }

    public Transaction() {
    }
}
