package rait.swedbe.model.accounts;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;


@Entity
@Table(name = "ACCOUNT")
public class Account {
    @Id
    @GeneratedValue( strategy=GenerationType.AUTO )
    @Column(unique = true)
    private Long id;

    @Setter @Getter
    @Column(unique = true)
    private String accountNumber;

    @Setter @Getter
    @Column
    private String accountOwner;

    @Setter @Getter
    @Column
    private double balance;

    @Setter @Getter
    @Column
    private String currency;

    @Setter @Getter
    @Column
    private int status;
}
