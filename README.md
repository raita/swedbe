Assignment
--
You are assigned to create an application for managing bank accounts. You can assume that this application is just for one user and no authentication/authorization has to be in place. Whoever is using the application should be able to do everything.

* It must be possible to create a new bank account, edit and delete it. For each account it must be possible to specify account name, balance and currency. Make it possible to choose at least between two currencies.
* It must be possible to view all created accounts.
* It should also be possible to make payments between accounts. 
    * When accounts have different currencies, then the money should be converted from source account currency to the currency that is used in target account. 
    * When making payments you should select source account, target account and the amount you want to transfer from the source account.
* It must not be possible to transfer more money than is available in account, also zero or negative amount.
 
Technical requirements
--
* Use Gradle or Maven (use dependencies from public repositories)
* Java 8+
* Use spring boot (web and data-jpa)
* Store data to h2 database (or some other embedded java sql dbms) but it should be possible to swap the data storage to something else (mysql, oracle etc)
* Everything has to go through REST api, do not use server side templating (jsp, thymeleaf etc)
* For client side use Angular
 
Tests
--
Use JUnit for tests
 
Publishing
--
Upload your code to public version control repository, for example free GitHub repository.